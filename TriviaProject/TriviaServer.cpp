#pragma once 
#include <thread>
#include "TriviaServer.h"
#include "Protocol.h"
#include "Helper.h"
#include "Validator.h"
#include <stdlib.h>
#include <iostream>


int TriviaServer::_roomIdSequence;

TriviaServer::TriviaServer() : _db(*(new DataBase())) //creating the socket and the database
{
	_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}

	_roomIdSequence = 0;
}
TriviaServer::~TriviaServer()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...)
	{
	}
	delete &_db;
}

void TriviaServer::server()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecieveMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers

		SOCKET client_socket = accept(_socket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		// create new thread for client	and detach from it
		std::thread tr(&TriviaServer::clientHandler, this, client_socket);
		tr.detach();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
}

void TriviaServer::clientHandler(SOCKET client_socket)
{

	RecievedMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(client_socket);
		while (msgCode != Protocol::CLIENT_EXIT)
		{
			currRcvMsg = buildRecieveMessage(client_socket, msgCode);
			addRecievedMessage(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(client_socket);
		}

		currRcvMsg = buildRecieveMessage(client_socket, Protocol::CLIENT_EXIT);
		addRecievedMessage(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		//dstd::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currRcvMsg = buildRecieveMessage(client_socket, Protocol::CLIENT_EXIT);
		addRecievedMessage(currRcvMsg);
	}
	closesocket(client_socket);
}


void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	User* user = getUserBySocket(msg->getSock());
	if (!user)
	{
		return;
	}
	try
	{
		for (auto it = _connectedUsers.cbegin(); it != _connectedUsers.cend(); )
		{
			if (it->second == user)
			{
				_connectedUsers.erase(it++);
				std::cout << "REMOVED %d, %s from clients list " << user->getSocket() << user->getUserName() << endl;
			}
			else
			{
				++it;
			}
		}
	}
	catch (...) {}
}

User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	std::string clientusername = msg->getValues()[0];
	std::string clientPassword = msg->getValues()[1];

	if (_db.isUserAndPassMatch(clientusername, clientPassword)) //if the user is registered
	{
		User* user = getUserByName(clientusername); //if the client is already connected 
		if (user)
		{
			Helper::sendData(msg->getSock(), std::to_string(Protocol::SIGNIN_STATE) + "2"); //user is already connected
			return NULL;
		}
		return new User(clientusername, msg->getSock()); //the new user
	}
	else
	{
		Helper::sendData(msg->getSock(), std::to_string(Protocol::SIGNIN_STATE) + "1"); //wrong details
		return NULL;
	}
}
bool TriviaServer::handleSingup(RecievedMessage* msg)
{
	std::string clientusername = msg->getValues()[0];
	std::string clientPassword = msg->getValues()[1];
	std::string clientEmail = msg->getValues()[2];

	try
	{
		if (!Validator::isUsernameValid(clientusername))
		{
			Helper::sendData(msg->getSock(), std::to_string(Protocol::SIGNUP_STATE) + "3"); //invalid username
			return false;
		}
		if (!Validator::isPasswordValid(clientPassword))
		{
			Helper::sendData(msg->getSock(), std::to_string(Protocol::SIGNUP_STATE) + "1"); //invalid password
			return false;
		}
		if (_db.isUserExists(clientusername)) //if the user isnt registered already
		{
			Helper::sendData(msg->getSock(), std::to_string(Protocol::SIGNUP_STATE) + "2"); //username already exists
			return false;
		}
		_db.addNewUser(clientusername, clientPassword, clientEmail);
		Helper::sendData(msg->getSock(), std::to_string(Protocol::SIGNUP_STATE) + "0"); //successful signup
		return true;
	}
	catch (...)
	{
		Helper::sendData(msg->getSock(), std::to_string(Protocol::SIGNUP_STATE) + "4"); //something went wrong
		return false;
	}
}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	safeDeleteUser(msg);
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	User* gamer = msg->getUser();

	if (gamer->leaveGame())
	{
		gamer->clearGame();
	}
}
void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	User* gamer = msg->getUser();
	Game* newGame = NULL;

	try
	{
		Room* playingRoom = gamer->getRoom();

		newGame = new Game(playingRoom->getUsers(), playingRoom->getQuestionsNo(), _db);
		_roomsList.erase(playingRoom->getId());
		gamer->closeRoom();

		gamer->setGame(newGame);
		newGame->sendFirstQuestion();
	}
	catch (...)
	{
		gamer->send(std::to_string(Protocol::SEND_QUESTION_WITH_POSSIBLE_ANSWERS) + "0"); //failure in creatng the game by the protocol
	}
}
void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if (user)
	{
		Game* game = user->getGame();
		if (game)
		{
			if (!game->handleAnswerFromUser(user, atoi(msg->getValues()[0].c_str()), atoi(msg->getValues()[1].c_str())))
			{
				user->clearGame();
			}
		}
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	std::string roomName = msg->getValues()[0];
	int playersNumber = atoi(msg->getValues()[1].c_str());
	int questionsNumber = atoi(msg->getValues()[2].c_str());
	int questionTimeInSec = atoi(msg->getValues()[3].c_str());
	User* admin = msg->getUser();
	if (admin)
	{
		if (admin->createRoom(++_roomIdSequence, roomName, playersNumber, questionTimeInSec, questionsNumber))
		{
			_roomsList.insert(std::make_pair(_roomIdSequence, admin->getRoom()));
			return true;
		}
	}
	return false;
}
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if (user)
	{
		int roomId = user->getRoom()->getId();
		if (user->closeRoom() != -1)
		{
			_roomsList.erase(roomId);
			return true;
		}
	}
	return false;
}
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if (user)
	{
		if (_roomsList.find(std::atoi(msg->getValues()[0].c_str())) != _roomsList.end()) //if the rooms exists in the _roomsList
		{
			if (user->joinRoom(_roomsList.find(std::atoi(msg->getValues()[0].c_str()))->second) != -1)
			{
				return true;
			}
		}
		else
		{
			//send msg
			Helper::sendData(msg->getSock(), std::to_string(Protocol::JOIN_ROOM_ANSWER) + "2"); //invalid password
		}
	}
	return false;
}
bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	User* leavingUser = msg->getUser();
	leavingUser->leaveRoom();
	return true;
}
void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	int roomID = atoi(msg->getValues()[0].c_str());
	try
	{
		Room* targetRoom = _roomsList[roomID];
		Helper::sendData(msg->getSock(), targetRoom->getUsersListMessage()); //sending the users in the room  to the client that request it
	}
	catch (...)
	{

	}

}
void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	std::string msgToSend = std::to_string(Protocol::SEND_ROOM_LIST);
	int roomsNum = _roomsList.size();
	msgToSend += Helper::getPaddedNumber(_roomsList.size(), 4);
	for (auto& room : _roomsList)
	{
		std::string roomName = room.second->getName();
		int r = roomName.length();
		msgToSend += Helper::getPaddedNumber(room.first, 4);
		msgToSend += Helper::getPaddedNumber(roomName.length(), 2);
		msgToSend += roomName;
	}

	Helper::sendData(msg->getSock(), msgToSend);
}

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	DataBase db;
	std::vector<std::string> temp;
	std::string message;

	temp = db.getBestScores();
	message.assign(std::to_string(Protocol::SEND_BEST_SCORES));
	message.append(std::to_string(temp[0].length())); //length of user 1
	message.append(temp[0]); //user name of user1
	message.append(temp[1]); //score of user 1

	message.append(std::to_string(temp[2].length())); //length of user 2
	message.append(temp[2]); //user name of user2
	message.append(temp[3]); //score of user 2

	message.append(std::to_string(temp[3].length())); //length of user 3
	message.append(temp[3]); //user name of user3
	message.append(temp[4]); //score of user 3
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	DataBase db;
	std::vector<std::string> temp;
	std::string message;

	temp = db.getPersonalStatus(msg->getUser()->getUserName());
	message.assign(std::to_string(Protocol::SEND_PERSONAL_STATUS));
	message.append(temp[0]); //number of games
	message.append(temp[1]); //number of right questions
	message.append(temp[2]); //number of wrong questions
	message.append(temp[3]); //number of avg time per question

	msg->getUser()->send(message);
}

void TriviaServer::handleRecieveMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	std::string userName;
	while (true)
	{
		try
		{
			std::unique_lock<std::mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_queRcvMessages.empty())
				continue;

			RecievedMessage* currMessage = _queRcvMessages.front();
			_queRcvMessages.pop();
			lck.unlock();

			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();

			switch (msgCode)
			{
			case Protocol::SIGNIN:
			{
				User* newUser = handleSignin(currMessage);
				if (newUser)
				{
					_connectedUsers.insert(std::make_pair(clientSock, newUser));
					Helper::sendData(currMessage->getSock(), std::to_string(Protocol::SIGNIN_STATE) + "0");
				}
				break;
			}
			case Protocol::SIGNUP:
			{
				handleSingup(currMessage);
				break;
			}
			case Protocol::SIGNOUT:
			{
				handleSignout(currMessage);
				break;
			}
			case Protocol::CREATE_ROOM:
			{
				handleCreateRoom(currMessage);
				break;
			}
			case Protocol::CLOSE_ROOM:
			{
				handleCloseRoom(currMessage);
				break;
			}
			case Protocol::JOIN_ROOM:
			{
				handleJoinRoom(currMessage);
				break;
			}
			case Protocol::GET_USERS_IN_ROOM:
			{
				handleGetUsersInRoom(currMessage);
				break;
			}
			case Protocol::GET_ROOMS:
			{
				handleGetRooms(currMessage);
				break;
			}
			case Protocol::LEAVE_ROOM:
			{
				handleLeaveRoom(currMessage);
				break;
			}
			case Protocol::START_GAME:
			{
				handleStartGame(currMessage);
				break;
			}
			case Protocol::LEAVE_GAME:
			{
				handleLeaveGame(currMessage);
				break;
			}
			case Protocol::CLIENT_ANSWER:
			{
				handlePlayerAnswer(currMessage);
				break;
			}
			case Protocol::GET_BEST_SCORES:
			{
				handleGetBestScores(currMessage);
				break;
			}
			case Protocol::GET_PERSONAL_STATUS:
			{
				handleGetPersonalStatus(currMessage);
				break;
			}
			case Protocol::CLIENT_EXIT:
			{

			}
			default:
				break;
			}

			delete currMessage;
		}
		catch (...)
		{

		}
	}
}
void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	std::unique_lock<std::mutex> lck(_mtxRecievedMessages);
	_queRcvMessages.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();
}
RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessage* msg = nullptr;
	std::vector<std::string> values;
	if (msgCode == NULL)
	{
		return msg;
	}
	if (msgCode == Protocol::SIGNIN || msgCode == Protocol::SIGNUP)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		std::string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		values.push_back(userName);
		int passwordSize = Helper::getIntPartFromSocket(client_socket, 2);
		std::string password = Helper::getStringPartFromSocket(client_socket, passwordSize);
		values.push_back(password);
		/*if sign up the email should be value as well*/
		if (msgCode == Protocol::SIGNUP) //only in sign email exsicts
		{
			int emailSize = Helper::getIntPartFromSocket(client_socket, 2);
			std::string email = Helper::getStringPartFromSocket(client_socket, emailSize);
			values.push_back(email);
		}
		msg = new RecievedMessage(client_socket, msgCode, values);
	}
	else if (msgCode == Protocol::GET_USERS_IN_ROOM || msgCode == Protocol::JOIN_ROOM)
	{
		std::string roomID = Helper::getStringPartFromSocket(client_socket, 4);
		values.push_back(roomID);
		msg = new RecievedMessage(client_socket, msgCode, values);
	}
	else if (msgCode == Protocol::CREATE_ROOM)
	{
		int roomNameSize = Helper::getIntPartFromSocket(client_socket, 2);
		std::string roomName = Helper::getStringPartFromSocket(client_socket, roomNameSize);
		values.push_back(roomName);
		int playersNumber = Helper::getIntPartFromSocket(client_socket, 1);
		values.push_back(std::to_string(playersNumber));
		int questionsNumber = Helper::getIntPartFromSocket(client_socket, 2);
		values.push_back(std::to_string(questionsNumber));
		int questionTimeInSec = Helper::getIntPartFromSocket(client_socket, 2);
		values.push_back(std::to_string(questionTimeInSec));
		msg = new RecievedMessage(client_socket, msgCode, values);
	}
	else if (msgCode == Protocol::CLIENT_ANSWER)
	{
		string answerNumber = std::to_string(Helper::getIntPartFromSocket(client_socket, 1));
		string timeInSeconds = std::to_string(Helper::getIntPartFromSocket(client_socket, 2));
		values.push_back(answerNumber);
		values.push_back(timeInSeconds);
		msg = new RecievedMessage(client_socket, msgCode, values);
	}
	else
	{
		msg = new RecievedMessage(client_socket, msgCode);
	}
	msg->setUser(getUserBySocket(msg->getSock())); //to bind a user to his message by the socket
	return msg;
}


User* TriviaServer::getUserByName(std::string username)
{
	for (auto user : _connectedUsers)
	{
		if (user.second->getUserName() == username)
		{
			return user.second;
		}
	}
	return NULL;
}
User* TriviaServer::getUserBySocket(SOCKET socket)
{
	for (auto user : _connectedUsers)
	{
		if (user.first == socket)
		{
			return user.second; //the user (pair)
		}
	}
	return NULL;
}
Room* TriviaServer::getRoomById(int id) { return NULL; }