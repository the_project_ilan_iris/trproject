#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sock, int messageCode) 
{
	_sock = sock;
	_messageCode = messageCode;
	_user = NULL;
}

RecievedMessage::RecievedMessage(SOCKET sock, int messageCode, std::vector<std::string> values)
{
	_sock = sock;
	_messageCode = messageCode;
	this->_values = values;
	_user = NULL;
}

SOCKET RecievedMessage::getSock()
{
	return _sock;
}
int RecievedMessage::getMessageCode() 
{
	return _messageCode;
}
User* RecievedMessage::getUser()
{
	return _user;
}
void RecievedMessage::setUser(User* user) 
{
	_user = user;
}
std::vector<std::string>& RecievedMessage::getValues()
{
	return _values;
}
