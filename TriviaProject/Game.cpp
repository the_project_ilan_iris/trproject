#include "Game.h"
#include "DataBase.h"
#include <iostream>
#include "Helper.h"
#include "Protocol.h"

Game::Game(const std::vector<User*>& players, int questionsNo, DataBase &db) : _db(db)
{
	try
	{
		_id = _db.insertNewGame();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << '\n';
	}

	_questions = _db.initQuestions(questionsNo);
	_players = players;

	for (int i = 0; i < players.size(); i++)
	{
		_players[i]->setGame(this);
		_results.insert(std::pair<std::string, int>(players[i]->getUserName(), 0)); //at the begining of the game there are no points
	}

}

Game::~Game()
{
	for (int i = 0; i < _questions.size(); i++)
	{
		free(_questions[i]);
	}
	_questions.clear();
	_players.clear();
}

void Game::sendQuestionToAllUsers()
{
	std::string *answers;

	std::string message = std::to_string(Protocol::SEND_QUESTION_WITH_POSSIBLE_ANSWERS); //the 118 request
	message.append(Helper::getPaddedNumber(_questions[_currQuestionIndex]->getQuestion().length(), 3)); //the length of the question

	try
	{
		if (_questions[_currQuestionIndex]->getQuestion().length() == 0) //fail because the length of the question is 0
		{
			for (int i = 0; i < _players.size(); i++)
			{
				_players[i]->send(Protocol::SEND_QUESTION_WITH_POSSIBLE_ANSWERS + "0"); //sending 1180 for fail
			}
		}
		else
		{
			message.append(_questions[_currQuestionIndex]->getQuestion()); //the question
			answers = _questions[_currQuestionIndex]->getAnswers(); //saving the answers;
			message.append(Helper::getPaddedNumber(answers[0].length(), 3)); //the length of the first ans
			message.append(answers[0]); //the first ans
			message.append(Helper::getPaddedNumber(answers[1].length(), 3)); //the length of the second ans
			message.append(answers[1]); //the second ans
			message.append(Helper::getPaddedNumber(answers[2].length(), 3)); //the length of the third ans
			message.append(answers[2]); //the third ans
			message.append(Helper::getPaddedNumber(answers[3].length(), 3)); //the length of the fourth ans
			message.append(answers[3]); //the fourth ans

			for (int i = 0; i < _players.size(); i++)
			{
				_players[i]->send(message); //sending 118 to all the players
			}
		}
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << '\n';
	}

	_currentTurnAnswers = 0;

}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	_db.updateGamesStatus(_id);

	try
	{
		for (int i = 0; i < _players.size(); i++)
		{
			std::string message = std::to_string(Protocol::SEND_GAME_OVER); //the 121 request
			message.append(Helper::getPaddedNumber(_players.size(),1)); //the number of the players
			message.append(Helper::getPaddedNumber(_players[i]->getUserName().length(),2)); //the username length
			message.append(_players[i]->getUserName()); //the user name
			message.append(Helper::getPaddedNumber(_results[_players[i]->getUserName()],2)); //the score of the user
			_players[i]->send(message); //sending the user the 121 request
			message.clear(); //clearing the message because every user has different data
			_players[i]->leaveGame(); //changing the currGame to nullptr
		}
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << '\n';
	}
}

bool Game::handleNextTurn()
{
	if (_players.empty()) //if there are players in the room
	{
		handleFinishGame();
		return false;
	}
	else
	{
		if (_currentTurnAnswers == _players.size()) //everyone answered
		{
			if (++_currQuestionIndex == _questions.size()) //if it was the last question
			{
				handleFinishGame();
				return false;
			}
			else
			{
				sendQuestionToAllUsers();
				return true;
			}
		}
	}
}

bool Game::handleAnswerFromUser(User *user, int answerNo, int time)
{
	std::string message = std::to_string(Protocol::SEND_ANSWER_INDICATOR);
	_currentTurnAnswers++;

	if (_questions.at(_currQuestionIndex)->getCorrectAnswerIndex() == answerNo) //if the player chose the right answer
	{
		_results.find(user->getUserName())->second++; //incresing the score of the player because he answered the right answer
		_db.addAnswerToPlayer(_id, user->getUserName(), _questions.at(_currQuestionIndex)->getId(), _questions.at(_currQuestionIndex)->getAnswers()[answerNo], true, time);
		message.append("1");
	}
	else
	{
		message.append("0");
	}
	if (answerNo == 5) //if the user didnt manage to answer on time/
	{
		_db.addAnswerToPlayer(_id, user->getUserName(), _questions.at(_currQuestionIndex)->getId(), "0" ,false, time);
	}




	user->send(message); //sending the 120 request - if the answer is correct or not
	return handleNextTurn();
}

bool Game::leaveGame(User *currUser)
{
	for (int i = 0; i < _players.size(); i++)
	{
		if (_players[i] == currUser)
		{
			_players.erase(_players.begin() + i); //erasing the current user from the vector
			return handleNextTurn();
		}
	}
}

int Game::getID()
{
	return _id;
}