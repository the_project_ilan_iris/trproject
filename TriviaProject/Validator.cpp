#include <vector>
#include <string>
#include <WinSock2.h>
#include "Validator.h"



//checking if the pass is valid - no spaces, length more than 4, at least one char uppercase and one char lowercase
bool Validator::isPasswordValid(std::string password)
{
	int upper = 0, lower = 0;

	if (password.length() < 4)
	{
		return false;
	}

	for (int i = 0; i < password.length(); i++)
	{
		if (isspace(password.at(i)))
		{
			return false;
		}

		if (isupper(password.at(i)))
		{
			upper++;
		}

		if (islower(password.at(i)))
		{
			lower++;
		}
	}

	if (upper >= 1 && lower >= 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// check if user name is valid - not null, starts with a char, no spaces
bool Validator::isUsernameValid(std::string userName)
{
	if (userName.length() == 0)
	{
		return false;
	}

	if (!isalpha(userName.at(0)))
	{
		return false;
	}

	for (int i = 0; i < userName.length(); i++)
	{
		if (isspace(userName.at(i)))
		{
			return false;
		}
	}
	return true;
}