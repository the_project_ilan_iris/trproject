#include "DataBase.h"
#include "Validator.h"
#include "sqlite3.h"
#include <exception>
#include <iostream>
#include <time.h>
#include <sstream>
#include <vector>
#include "Game.h"
#include <string>
#include <algorithm>
#include <chrono>
#include <random>
#include <iomanip>
#include "Helper.h"

std::string DataBase::dbAnswers;

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	DataBase* db = (DataBase*)notUsed;

	for (int i = 0; i < argc; i++)
	{
		db->dbAnswers.assign(std::string(argv[i]));
	}

	return 0;
}

DataBase::DataBase()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	try
	{
		rc = sqlite3_open("DataBase.db", &db);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << '\n';
	}

}

DataBase::~DataBase()
{
}



bool DataBase::isUserExists(std::string user)
{
	std::stringstream s;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	dbAnswers.clear();

	rc = sqlite3_open("DataBase.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
	}

	s.clear();
	
	s << "select username from t_users where username='" << user<<"'";
	std::string str = s.str();
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK || dbAnswers.empty())
	{
		return false; //the select command didnt work out - that means that the user doesnt exists
	}

	return true; //the user exists
}

bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	std::stringstream s;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	if (!(Validator::isUsernameValid(username) && Validator::isPasswordValid(password)))
	{
		return false;
	}
	if (isUserExists(username))
	{
		return false;
	}
	try
	{
		rc = sqlite3_open("DataBase.db", &db);
		if (rc)
		{
			std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
			sqlite3_close(db);
			system("Pause");
		}

		s.clear();
		s << "insert into t_users (username,password,email) values('" << username << "','" << password << "','" << email << "')";
		std::string str = s.str();
		rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "a problem in function addNewUser - creating a record\n";
		}


	}
	catch (...)
	{
		return false;
	}
	return true;
}

bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	std::stringstream s;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	if (isUserExists(username))
	{
		rc = sqlite3_open("DataBase.db", &db);
		if (rc)
		{
			std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
			sqlite3_close(db);
			system("Pause");
		}

		s.clear();
		s << "select password from t_users where username='" << username<<"'";
		std::string str = s.str();
		dbAnswers.clear();
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "a problem in function isUserAndPassMatch - selecting\n";
		}
		else
		{
			if (password == dbAnswers) //dbAnswers will containe the password from the table
			{
				return true;
			}
		}
	}
	return false;
}

std::vector<std::string> DataBase::getBestScores()
{
	std::stringstream s;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	int maxElemIndex = 0;
	std::vector<std::string> usersWithHighestScores;

	rc = sqlite3_open("DataBase.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
	}

	//need to sum the is_Correct of every user to get his score
	s << "select username, sum(is_correct) from t_players_answers group by username"; //selecting all the scores from the table
	std::string str = s.str();
	rc = sqlite3_exec(db, str.c_str(), &DataBase::callbackBestScores, (void*)this, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function getBestScores - selecting columns\n";
	}

	//getting the top 3 with the highest score
	for (int i = 0; i < _scores.size(); i++)
	{
		maxElemIndex = std::max_element(_scores.begin(), _scores.end()) - _scores.begin(); //finding the place of the highest num in the vector
		usersWithHighestScores.push_back(_scores[maxElemIndex].second);
		_scores.erase(_scores.begin() + maxElemIndex);
	}
	return usersWithHighestScores;
}

//TO DO:
std::vector<std::string> DataBase::getPersonalStatus(std::string username)
{
	std::stringstream s;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	std::vector<std::string> personalStatus;
	std::string::size_type sz;

	rc = sqlite3_open("DataBase.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
	}

	s << "select count(game_id) from t_players_answers where username=" << "'" << username << "'"; //the numbers of the games
	std::string str = s.str();
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function getPersonalStatus - selecting\n";
		personalStatus.push_back("0");
	}
	else
	{
		personalStatus.push_back(dbAnswers);
	}


	dbAnswers.clear();
	s.clear();
	s << "select sum(is_correct) from t_players_answers where is_correct=1 and username=" << "'" << username << "'"; //the right answers
	str = s.str();
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function getPersonalStatus - selecting\n";
		personalStatus.push_back("0");
	}
	else
	{
		personalStatus.push_back(dbAnswers);
	}

	s.clear();
	dbAnswers.clear();
	s << "select sum(is_correct) from t_players_answers where is_correct=0 and username=" << "'" << username << "'"; //the wrong answers
	str = s.str();
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function getPersonalStatus - selecting\n";
		personalStatus.push_back("0");
	}
	else
	{
		personalStatus.push_back(dbAnswers);
	}

	s.clear();
	dbAnswers.clear();
	s << "select avg(answer_time) from t_players_answers where username=" << "'" << username << "'"; //the avg time of the questions
	str = s.str();
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function getPersonalStatus - selecting\n";
		personalStatus.push_back("0");
	}
	else
	{
		float avgTimeAns = std::stof(dbAnswers, &sz);
		std::stringstream stream;
		stream << std::fixed << std::setprecision(2) << avgTimeAns; //after the dote there will be 2 numbers
		stream >> avgTimeAns;
		Helper help;
		std::string temp = help.getPaddedNumber((int)avgTimeAns, 4); // the number with or without zero (like 3.14 -> 0314)
		personalStatus.push_back(temp);
	}

	return personalStatus;
}

int DataBase::insertNewGame()
{
	time_t  timev;
	std::stringstream s, s1;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	timev = time(&timev);
	rc = sqlite3_open("DataBase.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	s << "insert into t_games (status,start_time) values(0," << timev << ")";
	std::string str = s.str();
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function insertNewGame - creating a record\n";
	}

	dbAnswers.clear();
	s1 << "select game_id from t_games where start_time=" << timev;
	std::string str1 = s1.str();
	rc = sqlite3_exec(db, str1.c_str(), callback, (void*)this, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function insertNewGame - pulling the game_id\n";
	}
	return std::atoi(dbAnswers.c_str()); //the game_id
}

std::vector<Question*> DataBase::initQuestions(int questionNo)
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	std::vector<Question*> tempQuestions;
	DataBase temp;

	rc = sqlite3_open("DataBase.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
	}


	rc = sqlite3_exec(db, "select * from t_questions", &DataBase::callbackQuestions, (void*)this, &zErrMsg); //getting all the information from the table to init the question fields
	if (rc == SQLITE_OK)
	{
		auto it = _questions.begin();
		tempQuestions.assign(it, _questions.begin() + questionNo); //copying the number(questionNo) from _questions

																   // obtain a time-based seed:
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::shuffle(begin(tempQuestions), end(tempQuestions), std::default_random_engine(seed));

		return tempQuestions;
	}


}

bool DataBase::updateGamesStatus(int idGame)
{
	time_t  timev;
	std::stringstream s;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("DataBase.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	s << "update t_games set status=1,start_time=datetime('now') where game_id=" << idGame; //change the status of the game in the id that is given
	std::string str = s.str();
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{

		std::cout << "a problem in function updateGamesStatus - updating a record\n";
		return false;
	}

	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, std::string userName, int question, std::string answer, bool isCorrect, int answerTime)
{
	std::stringstream s;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("DataBase.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	s << "insert into t_players_answers (game_id,username,question_id,player_answer,is_correct,answer_time) values(" << gameId + 1 << ",'" << userName
		<< "'," << question << ",'" << answer << "'," << isCorrect << "," << answerTime << ")"; //creating new record
	std::string str = s.str();
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "a problem in function addAnswerToPlayer - insert a record\n";
		return false;
	}

	return true;
}

int DataBase::callbackCount(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackBestScores(void *nothing, int argc, char ** argv, char ** azCol)
{
	DataBase* db = (DataBase*)nothing;
	std::pair<int, std::string> temp;

	for (int i = 0; i < argc; i++)
	{
		temp = std::make_pair(atoi(argv[1]), argv[0]); //1 = score, 0 = username
		db->_scores.push_back(temp);
	}

	return 0;
}

int DataBase::callbackPersonalStatus(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackQuestions(void *nothing, int argc, char **argv, char **azCol)
{
	DataBase* db = (DataBase*)nothing;
	int j = 0;

	Question* q = new Question(atoi(argv[0]), std::string(argv[2]), std::string(argv[3]), std::string(argv[4]), std::string(argv[5]), std::string(argv[1]));
	db->_questions.push_back(q);

	return 0;
}
