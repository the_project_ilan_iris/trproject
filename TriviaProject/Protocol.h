#pragma once



class Protocol
{
public:
	//Client-Server
	static const int SIGNIN = 200;
	static const int SIGNOUT = 201;
	static const int SIGNUP = 203;
	static const int GET_ROOMS = 205;
	static const int GET_USERS_IN_ROOM = 207;
	static const int JOIN_ROOM = 209;
	static const int LEAVE_ROOM = 211;
	static const int CREATE_ROOM = 213;
	static const int CLOSE_ROOM = 215;
	static const int START_GAME = 217;
	static const int CLIENT_ANSWER = 219;
	static const int LEAVE_GAME = 222;
	static const int GET_BEST_SCORES = 223;
	static const int GET_PERSONAL_STATUS = 255;
	static const int CLIENT_EXIT = 299;
	
	//Server-Client
	static const int SIGNIN_STATE = 102;
	static const int SIGNUP_STATE = 104;
	static const int SEND_ROOM_LIST = 106;
	static const int SEND_USERS_IN_ROOM = 108;
	static const int JOIN_ROOM_ANSWER = 110;
	static const int LEAVE_ROOM_ANSWER = 112;
	static const int CREATE_ROOM_ANSWER = 114;
	static const int CLOSE_ROOM_ANSWER = 116;
	static const int SEND_QUESTION_WITH_POSSIBLE_ANSWERS = 118;
	static const int SEND_ANSWER_INDICATOR = 120;
	static const int SEND_GAME_OVER = 121;
	static const int SEND_BEST_SCORES = 124;
	static const int SEND_PERSONAL_STATUS = 126;
};