#pragma once
#include <map>
#include <vector>

#include "Question.h"
#include "DataBase.h"
#include "User.h"

class User;

class Game
{
private:
	std::vector<Question*> _questions;
	std::vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	std::map<std::string, int> _results;
	int _id;
	int _currentTurnAnswers;

public:
	Game(const std::vector<User*>& players, int questionsNo, DataBase &db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User *user, int answerNo, int time);
	bool leaveGame(User* currUser);
	int getID();
	void sendQuestionToAllUsers();

};

