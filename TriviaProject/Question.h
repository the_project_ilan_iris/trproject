#pragma once 
#include <string>

class Question
{
private:
	std::string _question;
	std::string _answers[4];
	int _correctAnswerIndex;
	int _id;

public:
	Question(int id, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4, std::string question);
	std::string getQuestion();
	std::string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();
};