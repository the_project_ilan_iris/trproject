#pragma once

#include <string>
#include <vector>
#include "User.h"


class User;

class Room
{
public:
	Room(int id, User* admin, std::string name, int maxUsers, int questionTime, int questionsNo);
	~Room();
	bool joinRoom(User* admin);
	void leaveRoom(User* admin);
	int closeRoom(User* user);
	std::string getUsersAsString();
	std::vector<User*> getUsers();
	std::string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	std::string getName();

private:
	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	std::string _name;
	int _id;

	std::string getUsersAsString(std::vector<User*> users, User* admin);
	void sendMessage(std::string message);
	void sendMessage(User* user, std::string message);
};