#pragma once

#include <string>
#include <vector>
#include "User.h"



class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int messageCode);

	RecievedMessage(SOCKET sock, int messageCode, std::vector<std::string> values);

	SOCKET getSock();
	int getMessageCode();
	User* getUser();
	void setUser(User* );
	std::vector<std::string>& getValues();

private:
	SOCKET _sock;
	int _messageCode;
	std::vector<std::string> _values;
	User* _user;
};