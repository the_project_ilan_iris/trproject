#pragma once

#include <vector>
#include <string>
#include <map>
#include "Question.h"

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(std::string user);
	bool addNewUser(std::string, std::string, std::string);
	bool isUserAndPassMatch(std::string, std::string);
	std::vector<Question*> initQuestions(int questionNo);
	std::vector<std::string> getBestScores();
	std::vector<std::string> getPersonalStatus(std::string);
	int insertNewGame();
	bool updateGamesStatus(int idGame);
	bool addAnswerToPlayer(int gameId, std::string userName, int question, std::string answer, bool isCorrect, int answerTime);

	int static callbackCount(void*, int, char**, char**);
	int static callbackQuestions(void*, int, char**, char**);
	int static callbackBestScores(void*, int, char**, char**);
	int static callbackPersonalStatus(void*, int, char**, char**);

	std::string static dbAnswers;
	std::vector<std::pair<int/*score*/, std::string/*username*/>> _scores;

private:
	std::map<std::string/*username*/, std::pair<std::string/*email*/, std::string/*password*/>> _usersInDatabase;
	std::vector<Question*> _questions;

};