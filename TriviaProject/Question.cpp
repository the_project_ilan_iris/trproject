#include "Question.h"
#include <string>
#include <algorithm>
#include <chrono>
#include <random>

Question::Question(int id, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4, std::string question)
{
	_id = id;
	_question.assign(question);

	_answers[0] = correctAnswer;
	_answers[1] = answer2;
	_answers[2] = answer3;
	_answers[3] = answer4;

	// obtain a time-based seed:
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(begin(_answers), end(_answers), std::default_random_engine(seed));

	for (int i = 0; i < 4; ++i)
	{
		if (_answers[i] == correctAnswer)
		{
			_correctAnswerIndex = i;
			break;
		}
	}
}

std::string Question::getQuestion()
{
	return _question;
}

std::string* Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getId()
{
	return _id;
}