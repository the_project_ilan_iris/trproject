
#pragma once 
#include <map>
#include <condition_variable>
#include <mutex>
#include <queue>
#include "DataBase.h"
#include "RecievedMessage.h"
#include "Room.h"

class TriviaServer
{
	private:
	SOCKET _socket;
	std::condition_variable _msgQueueCondition;
	std::map<SOCKET, User*> _connectedUsers;
	DataBase& _db;
	std::map<int, Room*> _roomsList;
	static int _roomIdSequence;
	std::mutex _mtxRecievedMessages;
	std::queue<RecievedMessage*> _queRcvMessages;

	static const unsigned short PORT = 8820;
	static const unsigned int IFACE = 0;

	public:
	TriviaServer();
	~TriviaServer();
	
	void server();
	void bindAndListen();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessage*);

	User* handleSignin(RecievedMessage*);
	bool handleSingup(RecievedMessage*);
	void handleSignout(RecievedMessage*);

	void handleLeaveGame(RecievedMessage*);
	void handleStartGame(RecievedMessage*);
	void handlePlayerAnswer(RecievedMessage*);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersInRoom(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);

	void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);

	void handleRecieveMessages();
	void addRecievedMessage(RecievedMessage*);
	RecievedMessage * buildRecieveMessage(SOCKET, int);

	User* getUserByName(std::string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);
};