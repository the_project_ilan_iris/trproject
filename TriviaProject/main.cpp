
#include <vector>
#include <string>
#include "WSAInitializer.h"
#include <WinSock2.h>
#include "Validator.h"
#include "TriviaServer.h"
#include <iostream>



int main()
{
	//TRACE("Starting...");
	// NOTICE at the end of this block the WSA will be closed 
	WSAInitializer wsa_init;
	try
	{
		TriviaServer md_server;
		md_server.server();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << '\n';
	}

	if (Validator::isPasswordValid("Lmd1"))
	{
		std::cout << "the pass is valid" << std::endl;
	}
	else
	{
		std::cout << "the pass is not valid" << std::endl;
	}

	if (Validator::isUsernameValid("iris12"))
	{
		std::cout << "the user Name is valid" << std::endl;
	}
	else
	{
		std::cout << "the user Name is not valid" << std::endl;
	}
	//delete s;
	system("PAUSE");
	return 0;

}